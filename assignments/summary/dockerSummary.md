 Docker
 
**Who invented DOCKER:-**

Docker Inc. was founded by Solomon Hykes and Sebastien Pahl and it launched in 2011. Hykes started the Docker project in France as an internal project within dotCloud, a platform-as-a-service company.
Docker debuted to the public in Santa Clara at PyCon in 2013. It was released as open-source in March 2013.At the time, it used LXC as its default execution environment. One year later, with the release of version 0.9, Docker replaced LXC with its own component, which was written in the Go programming language.
In 2017, Docker created the Moby project for open research and development.

**Why DOCKER:-**

Developing apps today requires so much more than writing code. Multiple languages, frameworks, architectures, and discontinuous interfaces between tools for each lifecycle stage creates enormous complexity. Docker simplifies and accelerates your workflow, while giving developers the freedom to innovate with their choice of tools, application stacks, and deployment environments for each project.


**Introduction of  DOCKER:-**

Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels. 
 
**Containers:-**

- In 2013, Docker introduced what would become the industry standard for containers. Containers are a standardized unit of software that allows developers to isolate their app from its environment, solving the “it works on my machine” headache.

- For millions of developers today, Docker is the standard to build and share containerized apps - from desktop, to the cloud. We are building on our unique connected experience from code to cloud for developers and developer teams.

![ALT](https://gitlab.com/balanaguyashwanth/orientation/-/raw/master/assignments/summary/Assets/container.png)


- Containers are portable to any code of language in docker.Mainly docker is allows to create the container to write code in it & this container coming with many configurations, netwoking, and many dependencies to run our code very smoothly in any local machine.  
 
**Three mantras to get famous the docker :-**

-  Keep it simple
-  Move fast
-  Collaborate
 
**Quick overview of docker:-**
  
  ![ALT](https://www.intellectsoft.net/blog/wp-content/uploads/What-is-Docker.png)


Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and deploy it as one package.

By doing so, thanks to the container, the developer can rest assured that the application will run on any other Linux machine regardless of any customized settings that machine might have that could differ from the machine used for writing and testing the code.

In a way, Docker is a bit like a virtual machine. But unlike a virtual machine, rather than creating a whole virtual operating system, Docker allows applications to use the same Linux kernel as the system that they're running on and only requires applications be shipped with things not already running on the host computer.

This gives a significant performance boost and reduces the size of the application.
And importantly, Docker is open source. This means that anyone can contribute to Docker and extend it to meet their own needs if they need additional features that aren't available out of the box.
 
 
**How to use DOCKER to write programming :-**

- Initally, we have to check the visualization enabled or not in our local system before installation of docker, if not enabled it.

- Install ToolBox to run DOCKER

Then we have to install DOCKER via...

![ALT](https://gitlab.com/balanaguyashwanth/orientation/-/raw/master/assignments/summary/Assets/docker-installation.png)

- When we are creating the account of DOCKER we have to set the Id, email & password. 

- After finish the installation of DOCKER verify with "docker versionter" in your local-machine. 

- Open the terminal of your local system and then we have to run "docker run hello-world"

![ALT](https://gitlab.com/balanaguyashwanth/orientation/-/raw/master/assignments/summary/Assets/docker-communicarion.png)
 
- Once the commad run in the terminal, the DOCKER will will take the input from DOCKER CLIENT and then 
it passes to DOCKER DAEMON then it will check whether the "Local image storage" is having the library or not .

- When the library is not there in "Local image storage" it will import the libraries from "DOCKER HUB ONLINE" and then it exceutes the command successfully in the local system of client side machine.

**Architecture of VM & DOCKER**

![ALT](https://gitlab.com/balanaguyashwanth/orientation/-/raw/master/assignments/summary/Assets/VM.png)

![ALT](https://gitlab.com/balanaguyashwanth/orientation/-/raw/master/assignments/summary/Assets/dockerARCH.png)


**Installation of libraries using DOCKER commands**

In this i am explaining the install the some libraries like mongoDB in docker using in-built docker commands , Here we can say libraries as images in docker

- Initally we have to install the image of the library for example i am installing mongo 

> docker pull mongo

- Check the mongo is installed or not in your local machine by running below commad and collect the id of mongo, my id is  ba74783399

> docker ps --all

- After installation of mongo, run the mongo by below commad

> docker start ba74783399

- Check the mongo is running or not in your local machine

> docker ps --all

- To enter into the container we have to excute the below commad by this we can do some operations in mongoDB.

> docker exec -it ba74783399

As the installation of mongoDB we can do so many images installation of docker and work on it in very fast and simple way instead of spending the more and more time on installation of libraries, this is the beauty of docker.








  



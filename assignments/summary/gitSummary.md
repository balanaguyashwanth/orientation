**Who invented Git?**

Git development began in April 2005, after many developers of the Linux kernel gave up access to BitKeeper, a proprietary source-control management (SCM) system that they had been used to maintain the project since 2002. The copyright holder of BitKeeper, Larry McVoy, had withdrawn free use of the product after claiming that Andrew Tridgell had created SourcePuller by reverse engineering the BitKeeper protocols.The same incident also spurred the creation of another version-control system, Mercurial.

Linus Torvalds wanted a distributed system that he could use like BitKeeper, but none of the available free systems met his needs. Torvalds cited an example of a source-control management system needing 30 seconds to apply a patch and update all associated metadata, and noted that this would not scale to the needs of Linux kernel development, where synchronizing with fellow maintainers could require 250 such actions at once. For his design criterion, he specified that patching should take no more than three seconds, and added three more points
 
**Why Git?**

![ALT](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQIO3Y0DYdKA8d_Uv9BIBz8wcy7ZqJbd_xbUA&usqp=CAU)

Switching from a centralized version control system to Git changes the way your development team creates software. And, if you’re a company that relies on its software for mission-critical applications, altering your development workflow impacts your entire business.
So, If you are uploaded each and every file in git every time like file1.0,file2.0 etc When we lost all files from local machine, at that time we can collect the files form github to local machine by a single click “clone” the repository in it. 
After developing so many versions of file, When file 20.0 is getting trouble to execute it, then we will go to back file19.0 and collect that file from github repository and work on it. This system is known as version-control system
 
 
**Quick overview of Git?**

![ALT](https://gitlab.com/balanaguyashwanth/orientation/-/raw/master/assignments/summary/Assets/git.png)

Git is the most commonly used version control system. Git tracks the changes you make to files, so you have a record of what has been done, and you can revert to specific versions should you ever need to. Git also makes collaboration easier, allowing changes by multiple people to all be merged into one source. 
So regardless of whether you write code that only you will see, or work as part of a team, Git will be useful for you.

Git is software that runs locally. Your files and their history are stored on your computer. You can also use online hosts (such as GitHub or Bitbucket) to store a copy of the files and their revision history. Having a centrally located place where you can upload your changes and download changes from others, enable you to collaborate more easily with other developers. Git can automatically merge the changes, so two people can even work on different parts of the same file and later merge those changes without loosing each other’s work!

**Important points to know about GIT Internals :-**

- Git Repositories - Create a git repository in gitlab
- Here repository means  the collection of files and folders (code files) that you’re using git to track. It’s the big box you and your team throw your code into.

- Clone your git repository and add the extracted zip file to any folder

- Modify or update the files in that particular folder to track your files by git  

- Use "git add ." command to move staged file system 

- Here, staging step in git allows you to continue making changes to the working directory, and when you decide you  interact with version control, it allows you to record changes in small commits

- Commit Files -  commit your files by message to push your files to git repository and also more breifly the "commit" command is used to save your changes to the local repository. ... Using the "git commit" command only saves a new commit object in the local Git repository.

- Remote Repositories (on GitHub & Bitbucket)- To check authentication to your local and git

- Git push- All the files will uploaded to git repository

- Branches & Merging- Git lets you branch out from the original code base. This lets you more easily work with other developers, and gives you a lot of flexibility in your workflow. And you can merge local file to git and two branches also

- Pull Requests - Pull requests are a way to discuss changes before merging them into your codebase


**Git installation**

- If you are linux user 

- Initially refresh your files by using 

> sudo apt-get update

- Then open terminal and install git via using 

> sudo apt-get install git

- If you are a windows user go to https://git-scm.com/downloads and download the file there.

- If you are a mac user

> brew install git




**How to upload files from local machine to git repository using git commands**

![ALT](https://i.ytimg.com/vi/0nqJKEh3YCc/maxresdefault.jpg)



Initally we have to run very important commands git init and remote to connect the local machine file folder to git repository

> echo "# example" >> README.md

> git init

> git add README.md

> git commit -m "first commit"

> git remote add origin https://github.com/BalanaguYashwanth/example.git

To upload the existing file of local machine to git repository is 

> git push -u origin master

- You can clone the repository by using 

> git clone <link-to-repository> 
                 
Any new was created and in the local machine to add in the git we have to use:-

> git add filename

After adding the file to Staged location we have to commit 

> git commit -m "my first commit"

Then we can push completely the staged files to git repository by using 

> git push -u origin master

By using these type of commands we can create branches from master, pull requests and so on in  git




